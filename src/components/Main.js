export default function Main({ children }) {
	return (
		<>
			<main className="main-content main-content--grey">
				{children}
			</main>
		</>
	);
}
