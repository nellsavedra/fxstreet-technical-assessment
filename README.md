# FXStreet Technical Assessment
Git repo for the technical assessment @ FXStreet

## How to
Install dependencies & compile styles:

### `npm install`
Start our local dev server
### `npm run start`

## Demo
You can find the most recent build [here](https://fxstreet.pages.dev/)




